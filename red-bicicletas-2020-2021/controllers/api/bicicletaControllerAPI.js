/* ------------------------------------------------------- Controlador del API para bicicletas. -------------------------------------------------------*/

// Le decimos que modelo va a utilizar. 
let Bicicleta = require("../../models/Bicicleta");

// Se crea un listado con todas las bicicletas. 
exports.bicicleta_list = function(req, res) {
    // El listado se genera recurriendo al método allBicis, que nos hace busca todas las bicis en la base. 
    Bicicleta.allBicis(function (err, bicis) {
        // Si hay fallo lo saca. 
        if (err) res.status(500).send(err.message);
        // Si todo va bien nos saca un json con la información obtenida. 
        res.status(200).json({
            bicicletas: bicis
        });

    });

};

// Nos permite crear bicicletas.
exports.bicicleta_create = function(req, res) {
    // Definimos nuestra nueva bicicleta pasándole los parámetros por el body.  
    let bici = new Bicicleta({

        bicicletaID: req.body.bicicletaID,

        color: req.body.color,

        modelo: req.body.modelo,

        ubicacion: req.body.ubicacion

    });
    // Recurrimos al método add (añadir), para introducir la bici creada en nuestra base de datos. 
    Bicicleta.add(bici, function (err, bici) {
        // Si hay fallo lo saca. 
        if (err) res.status(500).send(err.message);
        // Si todo va bien nos saca un json con las bicicletas. 
        res.status(201).json({
            bicicletas: bici
        });

    });

};

// Nos permite el borrado de bicicletas. 
exports.bicicleta_delete = function(req, res) {
    // Recurrimos al método de borrado que tenemos, en este caso tendremos que pasarle el ID de la bici a borrar. 
    Bicicleta.removeById(req.body._id, function (err, result) {
        // Si hay fallo lo saca. 
        if (err) res.status(500).send(err.message);
        // Si todo va bien la borra a secas. 
        res.status(200).send();

    });

}

// Nos permite actualizar las bicicletas. 
exports.bicicleta_update = function(req, res) {
        // Cogemos el id a través del body y lo cargamos en una variable. 
        let idDocumento = req.body._id;
        /* Recurrimos al método que tenemos diseñado de actualización, le pasamos el id de la bicicleta a actualizar
        y los parámetros a cambiar del objeto. */
        Bicicleta.update(idDocumento, req.body, function (err, result) {
            // Si hay fallo lo saca.    
            if (err) res.status(500).send(err.message);
            // Si todo va bien nos devuelve la bici cambiada. 
            res.status(201).json(
                req.body
            );

        });

}