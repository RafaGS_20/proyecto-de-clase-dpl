/* ------------------------------------------------------- Controlador del API para bicicletas. -------------------------------------------------------*/

// Le decimos que modelo va a utilizar. 
let Reserva = require("../../models/Reserva");

// Permite la creación de las reservas. 
exports.reservas = function(req, res) {
    // Recurrismos al mecanimos de creación de reservas de la clase. 
    Reserva.reservas(function(err, reservas) {
        // Si hay fallo lo saca.
        if(err) res.status(500).send(err.message);
        // Si todo va bien saca un json con la reserva. 
        res.status(200).json(reservas);

    })

};