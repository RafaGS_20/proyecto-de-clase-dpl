/* ------------------------------------------------------- Controlador del API para usuarios. -------------------------------------------------------*/

// Le decimos que modelo va a utilizar. 
let Usuario = require("../../models/Usuario");

// Nos permite reservar bicicletas con usuario y bicicleta. 
exports.usuarios_reservar = function (req, res) {

    // Preguntar si es necesario definir la función como statics en el modelo.
    // Encuentra un usuario según su id pasado como parámetro. 
    Usuario.findById(req.body._id, function(err, usuario) {
        // Si hay fallo lo saca. 
        if(err) res.status(500).send(err.message);
        // Imprime en consola el usuario. 
        console.log(usuario);
        /* Usamos el método reservar con el usuario que tenemos de antes y le pasamos por parámetros
        los datos de la bicicleta a reservar y demás datos necesarios para crear reserva. */
        usuario.reservar(req.body.bici_id, req.body.desde, req.body.hasta, function(err) {
            // Si hay fallo lo saca.
            if(err) res.status(500).send(err.message);
            // Si todo va bien nos dice por consola que está reservado. 
            console.log("¡Reservada!");

            res.status(200).send();

        });

    });

};