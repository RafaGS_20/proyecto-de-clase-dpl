/* ------------------------------------------------------- Controlador de autetificación del token. -------------------------------------------------------*/

// Le decimos el modelo que va a usar. 
var Usuario = require('../../models/Usuario');

// Cargamos módulos necesarios. 
var bcrypt = require('bcrypt');
var jwt = require('jsonwebtoken');


// Exportamos todo: 
module.exports = { // Si el usuario que introducimos está correcto y es válido coge su token y lo cifra con la clave secreta que le dimos. 

 authenticate: function(req, res, next) {

 Usuario.findOne({email:req.body.email}, function(err, userInfo){

 if (err) {

 next(err);

 } else {

 if (userInfo===null) {

 return res.status(401).json({status:'error', message: "Correo o password incorrecto!", data:null});

 }

 if(userInfo !=null && bcrypt.compareSync(req.body.password, userInfo.password)) {

 var token = jwt.sign({id: userInfo._id}, req.app.get('secretkey'), {expiresIn: '7d'});

 res.status(200).json({message: 'Usuario encontrado!', data:{usuario:userInfo, token:token}});

 } else {

 res.status(401).json({status:'error', message:'Correo o clave incorrecta', data:null});

 }

 }

});

 }

}