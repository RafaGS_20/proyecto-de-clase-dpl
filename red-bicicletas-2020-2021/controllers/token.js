/* ------------------------------------------------------- Controlador para Tokens. -------------------------------------------------------*/

// Le decimos los modelos que debe usar. 
let Usuario = require("../models/Usuario"); // Usuario al que le daremos el token. 
let Token = require("../models/Token"); // Modelo que define el token a entregar. 

// Lo que va a exportar este controlador. 
module.exports = {
    // Confirmamos el token recurriendo a una función. 
    confirmationGet: function (req, res, next) {
        // Esta función buscará el token que se pasa por url con el usuario. 
        Token.findOne({ token: req.params.token }, function (err, token) {
            // Si no se encuentra el token será por no haber verificado el email: 
            if (!token) return res.status(400).send({ type: "not-verified", msg: "No se ha verificado el email" });
            // Si encontramos el token, buscamos al usuario por id con ese token asociado. 
            Usuario.findById(token._userId, function (err, usuario) {
                // Si no hay usuario asociado al token nos informa de ello: 
                if (!usuario) return res.status(400).send({ msg: "No encontramos un usuario con ese token" });
                // En caso de si existir nos redirecciona. 
                if (usuario.verificado) return res.redirect("/usuarios");
                // Convierte al usuario en usuario verificado. 
                usuario.verificado = true;
                // Guardia al usuario y su información pertinente. 
                usuario.save(function (err) {
                    // Si hay fallo nos lo saca. 
                    if (err) { return res.status(500).send({ msg: err.message }) }
                    // Sino nos redirige al inicio. 
                    res.redirect("/");
                });

            });

        });

    } 

};