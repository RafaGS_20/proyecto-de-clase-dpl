/* ------------------------------------------------------- Controlador para bicicletas. -------------------------------------------------------*/

// Le decimos que modelo va a utilizar. 
let Bicicleta = require("../models/Bicicleta");

// Obtiene la vista con la tabla de bicicletas.
exports.bicicleta_list = function (req, res) {
    // Usa el método allBicis para sacarlas todas de la base de datos. 
    Bicicleta.allBicis(function (err, result) {
        // Si hay fallo lo saca.
        if (err) res.status(500).send(err.message);
        // Si todo va bien se responde mandando la vista con las bicis encontradas. 
        res.render("bicicletas/index", {bicis: result});
    });
}

// Obtiene el formulario de inserción de nuevas bicicletas
exports.bicicleta_create_get = function (req, res) {
    res.render("bicicletas/create");
}

/* Crea un objeto de la clase Bicicleta, recogiendo por el método POST
y la añade al array de bicicletas que tenemos */
exports.bicicleta_create_post = function (req, res) {
    let bici = new Bicicleta({

        bicicletaID: req.body.bicicletaID,

        color: req.body.color,

        modelo: req.body.modelo,

        ubicacion: [req.body.latitud, req.body.longitud]

    });
    // Este método se utiliza para meter la bici nueva a la base. 
    Bicicleta.add(bici, function(err, bici) {
        // Si hay fallo lo saca. 
        if(err) res.status(500).send(err.message);
        // Redirección al index siguiendo el patrón de diseño PRG(POST-REDIRECT-GET)
        res.status(201).redirect("/bicicletas");
    });
}

// Método que obtiene el id de una bicicleta y permite borrarla. 
exports.bicicleta_delete_post = function (req, res) {
    // Borramos la bici a través de su id. 
    Bicicleta.removeById(req.body.id, function (err, result) {
        // Si hay fallo lo saca. 
        if (err) res.status(500).send(err.message);
        // Redirección al index siguiendo el patrón de diseño PRG(POST-REDIRECT-GET)
        res.redirect("/bicicletas");
    });
}

// Método que obtiene el id de una bici y la actualiza. 
exports.bicicleta_update_get = function (req, res) {
    // Encuentra la bici mediante su id. 
    Bicicleta.findById({_id: req.params.id}, function(err, result) {
        // Si hay fallo lo saca. 
        if(err) res.status(500).send(err.message);
        // Redirección al index siguiendo el patrón de diseño PRG(POST-REDIRECT-GET) 
        res.render("bicicletas/update", {bici: result});
    });
}

// Método que sube los datos del update y actualiza. 
exports.bicicleta_update_post = function (req, res) {
    // Cogemos el id a través de la URL. 
    let consulta = req.params.id;
    // Imprime por consola los datos del body. 
    console.log(req.body);
    // Actualizamos cogiendo la bici del id de la url. 
    Bicicleta.update(consulta, req.body, function(err, result) {
        // Si hay fallo lo saca. 
        if(err) res.status(500).send(err.message);
        // Al acabar nos dirige a la página principal. 
        res.redirect("/bicicletas");
    });
}