/* ------------------------------------------------------- Passport. -------------------------------------------------------
es un middleware que actua como sistema de autenticazión para nodejs. Funciona de forma modular y permite varias formas de 
logeo: local, login social, etc. */

// Creamos un passport diciéndole el módulo a usar. 
const passport = require("passport");

// Creamos LocalStrategy recurriendo el modulo local de verificación. 
const LocalStrategy = require("passport-local").Strategy; 

// Usamos al usuario definido en el modelo usuario. 
const Usuario = require("../models/Usuario");

// Aquí le decimos a passport que vamos a usar una nueva estrategia local que definimos a continuación: 
passport.use(new LocalStrategy(

    // Esta función coge el email y la contraseña y hace un callback devolviendo "done". 
    function(email, password, done) {

        // Dentro de los usuarios vamos al método buscaruno "findOne", para comprobar si existe un usuario con el email introducido. 
        // Como respuesta recurre a la otra función, que tiene la variable err para errores y usuario del findOne. 
        Usuario.findOne({email:email}, function(err, usuario) {
            // En caso de error lo devuelve. 
            if (err) return done(err);
            // En caso de no estar el usuario devuelve el mensaje siguiente: 
            if(!usuario) return done(null, false, {message: "Email inexistente o incorrecto"});
            // Si existe el usuario pero al pasarlo por validPassword la contraseña no es correcta, nos informan de ello: 
            if(!usuario.validPassword(password)) return done(null, false, {message: "Password incorrecto"});
            // Si todo sale bien no nos da mensaje y nos devuelve el usuario: 
            return done(null, usuario);

        });

    }

));


// Aquí se guarda el id del usuario para su uso y mantener su sesión mediante cookies. 
passport.serializeUser(function (user, cb) {

 cb(null, user.id);

});

// Aquí se busca el usuario por id para obtener su sesión y demás. 
passport.deserializeUser(function (id, cb) {

 Usuario.findById(id, function (err, usuario) {

 cb(err, usuario); 

 });

});

// Lo exportamos para uso. 
module.exports = passport;