/* ------------------------------------------------------- Modelo que define los token. -------------------------------------------------------*/

/* Este es el modelo token, en el definimos el objeto token, con sus
atributos y métodos para usarlos en el controlador y en nuestra app. */

// Llamamos a moment para el tema fecha y cálculos. 
let moment = require ("moment"); 
// Llamamos a mongoose para el trabajo con la base de datos. 
let mongoose = require ("mongoose"); 
let Schema = mongoose.Schema;

// Definimos el esquema con el token. 
let TokenSchema = new Schema ({

    _userId: {

        type: mongoose.Schema.Types.ObjectId, // Aquí le estamos diciendo que coja el id de otro objeto. 

        required: true, 

        ref: "Usuario"
    }, 

    token: {

        type: String, 

        required: true

    },

    createAt: {

        type: Date, 

        required: true, 

        default: Date.now, 

        expires: 43200
    }

}); 

// Exportamos todo para el controlador y demás. 
module.exports = mongoose.model("Token", TokenSchema); 