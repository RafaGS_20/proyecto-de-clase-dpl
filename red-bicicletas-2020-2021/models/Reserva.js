/* ------------------------------------------------------- Modelo que define las reservas. -------------------------------------------------------*/

/* Este es el modelo reserva, en el definimos el objeto reserva, con sus
atributos y métodos para usarlos en el controlador y en nuestra app. */

// Moment usado para temas de fechas y demás.
const moment = require ("moment"); 
// Mongoose para el tema base de datos.
let mongoose = require ("mongoose");
let Schema = mongoose.Schema; 

/* Creamos un esquema, que es lo que usa mongoose para trabajar, para definir
nuestro objeto reserva. */
let reservaSchema = new Schema 
({
    desde: Date, // Fecha de inicio de la reserva.

    hasta: Date, // Fecha fin de la reserva. 

    /* Al decirle "type:..." le estamos avisando de que ese parámetro es un objeto de la base de 
    datos, que tiene una id y que puede encontrar en el modelo bicicleta de la misma carpeta. */
    bicicleta: {type: mongoose.Schema.Types.ObjectId, ref: "Bicicleta"}, 

    usuario: {type: mongoose.Schema.Types.ObjectId, ref: "Usuario"}
}); 

// Método de las reservas: 
let Reserva = function(desde,hasta,bicicleta,usuario) {

    this.desde = desde;

    this.hasta = hasta;

    this.bicicleta = bicicleta;

    this.usuario = usuario; 

};

// Método que nos devuelve los días que estará reservada la bicicleta. 
reservaSchema.methods.diasDeReserva = function () 
{
    return moment (this.hasta.diff(moment(this.desde), "days") + 1); 
    // El método recurre al moment para calcular la diferencia de fechas. 
}

// Muestra todas las reservas que existen. 
reservaSchema.static.reservas = function(cb) {
    
    return this.find({}, cb);

}

// Exporta todo para fuera. 
module.exports = mongoose.model("Reserva", reservaSchema); 
