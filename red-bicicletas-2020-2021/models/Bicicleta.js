/* ------------------------------------------------------- Modelo que define las bicicletas. -------------------------------------------------------*/

/* Este es el modelo Bicicleta, en el definimos el objeto bicicleta, con sus
atributos y métodos para usarlos en el controlador y en nuestra app. */

// Primero le metemos el mongoose. 
let mongoose = require('mongoose');
// Creamos un esquema tipo mongoose para la base de datos.  
let Schema = mongoose.Schema;

// Definimos como es este esquema y que propiedades tendrán las bicicletas:
let bicicletaSchema = new Schema({

    bicicletaID: Number,

    color: String,

    modelo: String,

    ubicacion: { type: [Number], index: true }

});

// Definimos el objeto bicicleta y sus propiedades. 
let Bicicleta = function (id, color, modelo, ubicacion) {
    
    this.id = id;
    
    this.color = color;

    this.modelo = modelo;

    this.ubicacion = ubicacion;

}

// Obtiene un documento con todas las bicis:
bicicletaSchema.statics.allBicis = function (cb)  {

    return this.find({}, cb);

};

// Añade una bici a la colección de bicis:
bicicletaSchema.statics.add = function (aBici, cb) {

    return this.create(aBici, cb);

};

// Encuentra una bici en la colección mediante el ID:
bicicletaSchema.statics.findById = function (id, cb) {

    return this.findOne(id, cb);

};

// Elimina una bici de la colección mediante el ID:
bicicletaSchema.statics.removeById = function (id, cb) {

    return this.findByIdAndDelete(id, cb);

};

// Permite modificar una bici mediante su id: 
bicicletaSchema.statics.update = function (idDocumento, documento, cb) {

    return this.findByIdAndUpdate(idDocumento, documento, cb);

};

// Exportamos el modelo: 
module.exports = mongoose.model("Bicicleta", bicicletaSchema);