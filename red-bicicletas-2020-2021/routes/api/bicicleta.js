/* ------------------------------------------------------- API de las bicicletas. -------------------------------------------------------*/

// Le decimos todo lo que vamos a necesitar. 
let express = require('express');
let router = express.Router();
let bicicletaControllerAPI = require("../../controllers/api/bicicletaControllerAPI");

// Aquí definimos las rutas a tomar para cada proceso y los métodos que usarán. 
router.get("/", bicicletaControllerAPI.bicicleta_list);
router.post("/create", bicicletaControllerAPI.bicicleta_create);
router.delete("/delete", bicicletaControllerAPI.bicicleta_delete);
router.put("/update", bicicletaControllerAPI.bicicleta_update);

// Exportamos todo. 
module.exports = router;