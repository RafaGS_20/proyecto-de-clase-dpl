/* ------------------------------------------------------- Enrutador del inicio. -------------------------------------------------------*/

// Le decimos todo lo que vamos a necesitar.
var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

// Exportamos todo. 
module.exports = router;
