/* ------------------------------------------------------- Enrutador de autetificación del token. -------------------------------------------------------*/

// Cargamos recursos necesarios. 
var express = require('express'); 
var router = express.Router();

// Le pasamos el modelo que va a usar. 
var authControllerAPI = require('../../controllers/api/authControllerAPI');
var authController = require('../../controllers/api/authControllerAPI');

// Definimos la ruta. 
router.post('/authenticate', authControllerAPI.authenticate);

// Exportamos todo. 
module.exports = router;