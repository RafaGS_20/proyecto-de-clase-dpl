/* ------------------------------------------------------- Enrutador de bicicletas. -------------------------------------------------------*/

// Le decimos todo lo que vamos a necesitar.
var express = require('express');
var router = express.Router();
// Le decimos que suba una carpeta, se vaya a controllers y use el de bicicleta.
let bicicletaController = require('../controllers/bicicleta');

// Cuando en la ruta pilla /bicicletas activa el bicicleta.list del controlador. 
router.get("/", bicicletaController.bicicleta_list);

// Ruta para el create, el post y el delete. 
router.get("/create", bicicletaController.bicicleta_create_get);
router.get("/:id/update", bicicletaController.bicicleta_update_get);
router.post("/create", bicicletaController.bicicleta_create_post);
router.post("/:id/delete", bicicletaController.bicicleta_delete_post);
router.post("/:id/update", bicicletaController.bicicleta_update_post);

// Exportamos todo. 
module.exports = router; 
